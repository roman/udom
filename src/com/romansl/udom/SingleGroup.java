package com.romansl.udom;

import com.romansl.ranges.Range;
import com.romansl.ranges.SingleItemRange;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

class SingleGroup extends Node {
    private final Node mChild;

    public SingleGroup(final String name, final Map<String, String> attributes, final String text, final Node child) {
        super(name, attributes, text);
        mChild = child;
    }

    @NotNull
    @Override
    public Node optNode(final String name) {
        return mChild.getName().equals(name) ? mChild : NONE;
    }

    @NotNull
    @Override
    public Node optNode(final int index) {
        return index == 0 ? mChild : NONE;
    }

    @NotNull
    @Override
    public Node getNode(final int index) {
        if (index == 0)
            return mChild;
        else
            throw new ArrayIndexOutOfBoundsException(index);
    }

    @Override
    public int getChildCount() {
        return 1;
    }

    @Override
    public Range<Node> range() {
        return new SingleItemRange<Node>(mChild);
    }
}
