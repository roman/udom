package com.romansl.udom;

import org.jetbrains.annotations.NotNull;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Generator extends State {
    public Generator() {
        super(null);
    }

    public Node build(final XmlPullParser parser) throws XmlPullParserException, IOException {
        nodes.clear();
        State current = this;

        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG: {
                    current = current.next();

                    current.name = parser.getName();

                    final int count = parser.getAttributeCount();
                    if (count == 0) {
                        current.attributes = Collections.emptyMap();
                    } else {
                        final Map<String, String> map = new HashMap<String, String>();
                        for (int i = 0; i < count; ++i) {
                            map.put(parser.getAttributeName(i), parser.getAttributeValue(i));
                        }

                        current.attributes = map;
                    }

                    break;
                }
                case XmlPullParser.TEXT: {
                    current.text = parser.getText();
                    break;
                }
                case XmlPullParser.END_TAG: {
                    final Node node = current.toNode();
                    current = current.prev;
                    current.nodes.add(node);
                    break;
                }
            }
            eventType = parser.next();
        }

        return toNode();
    }
}

class State {
    private State next;
    final State prev;
    @NotNull final List<Node> nodes = new ArrayList<Node>();
    @NotNull String name = "";
    @NotNull Map<String, String> attributes = Collections.emptyMap();
    @NotNull String text = "";

    State(final State prev) {
        this.prev = prev;
    }

    Node toNode() {
        if (nodes.size() == 0) {
            return new Leaf(name, attributes, text);
        } else if (nodes.size() == 1) {
            return new SingleGroup(name, attributes, text, nodes.get(0));
        } else {
            return new Group(name, attributes, text, nodes);
        }
    }

    State next() {
        if (next == null) {
            next = new State(this);
        } else {
            next.nodes.clear();
            next.text = "";
        }

        return next;
    }
}
