package com.romansl.udom;

import com.romansl.ranges.Range;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public abstract class Node {
    public static final Node NONE = new None();

    private final @NotNull String mName;
    private final @NotNull Map<String, String> mAttributes;
    private final @NotNull String mText;

    public Node(@NotNull final String name, @NotNull final Map<String, String> attributes, @NotNull final String text) {
        mName = name;
        mAttributes = attributes;
        mText = text;
    }

    public abstract @NotNull Node optNode(final String name);
    public abstract @NotNull Node optNode(final int index);
    public abstract @NotNull Node getNode(final int index);
    public abstract int getChildCount();
    public abstract Range<Node> range();

    public @NotNull String optAttr(final String name, final String defaultValue) {
        final String value = mAttributes.get(name);
        return value == null ? defaultValue : value;
    }

    public @NotNull String optAttr(final String name) {
        final String value = mAttributes.get(name);
        return value == null ? "" : value;
    }

    public long optLongAttr(final String name, final long defaultValue) {
        final String value = mAttributes.get(name);
        return value == null ? defaultValue : Long.parseLong(value);
    }

    public long getLongAttr(final String name) {
        return Long.parseLong(mAttributes.get(name));
    }

    public int optIntAttr(final String name, final int defaultValue) {
        final String value = mAttributes.get(name);
        return value == null ? defaultValue : Integer.parseInt(value);
    }

    public int getIntAttr(final String name) {
        return Integer.parseInt(mAttributes.get(name));
    }

    public boolean optBoolAttr(final String name, final boolean defaultValue) {
        final String value = mAttributes.get(name);
        return value == null ? defaultValue : "1".equals(value);
    }

    public boolean getBoolAttr(final String name) {
        return mAttributes.get(name).equals("1");
    }

    public @Nullable String getAttr(final String name) {
        return mAttributes.get(name);
    }

    public @NotNull String getName() {
        return mName;
    }

    public @NotNull Map<String, String> getAttributes() {
        return mAttributes;
    }

    public float getFloatAttr(final String name) {
        return Float.parseFloat(mAttributes.get(name));
    }

    public float optFloatAttr(final String name, final float defaultValue) {
        final String value = mAttributes.get(name);
        return value == null ? defaultValue : Float.parseFloat(value);
    }

    public <T> T asClass(final Class<T> clazz) {
        try {
            return clazz.getDeclaredConstructor(Node.class).newInstance(this);
        } catch (final InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (final NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (final InstantiationException e) {
            throw new RuntimeException(e);
        } catch (final IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @NotNull
    public <T> T[] asArrayOfClass(final Class<T> clazz) {
        final int count = getChildCount();
        //noinspection unchecked
        final T[] result = (T[]) Array.newInstance(clazz, count);

        for (int i = 0; i < count; ++i) {
            result[i] = getNode(i).asClass(clazz);
        }

        return result;
    }

    @NotNull
    public <T> T[] asArrayOfClassIgnoringErrors(final Class<T> clazz) {
        final Constructor<T> constructor;
        try {
            constructor = clazz.getDeclaredConstructor(Node.class);
        } catch (final NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        final int count = getChildCount();
        //noinspection unchecked
        final T[] result = (T[]) Array.newInstance(clazz, count);

        int i = 0;
        int j = 0;

        for (; i < count; ++i) {
            try {
                result[j] = constructor.newInstance(getNode(i));
                ++j;
            } catch (final Exception ignored) {

            }
        }

        if (i == j) {
            return result;
        } else {
            //noinspection unchecked
            final T[] finalResult = (T[]) Array.newInstance(clazz, j);
            System.arraycopy(result, 0, finalResult, 0, j);
            return finalResult;
        }
    }

    @NotNull
    public String getText() {
        return mText;
    }
}
