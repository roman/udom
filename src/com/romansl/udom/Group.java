package com.romansl.udom;

import com.romansl.ranges.ArrayRange;
import com.romansl.ranges.Range;

import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import java.util.Map;

class Group extends Node {
    private final Node[] mChildren;

    public Group(final String name, final Map<String, String> attributes, final String text, final Collection<Node> src) {
        super(name, attributes, text);

        mChildren = src.toArray(new Node[src.size()]);
    }

    @NotNull
    @Override
    public Node optNode(final String name) {
        for (final Node child : mChildren) {
            if (name.equals(child.getName())) {
                return child;
            }
        }

        return Node.NONE;
    }

    @NotNull
    @Override
    public Node optNode(final int index) {
        return mChildren.length > index ? mChildren[index] : Node.NONE;
    }

    @NotNull
    @Override
    public Node getNode(final int index) {
        return mChildren[index];
    }

    @Override
    public int getChildCount() {
        return mChildren.length;
    }

    @Override
    public Range<Node> range() {
        return new ArrayRange<Node>(mChildren);
    }
}
