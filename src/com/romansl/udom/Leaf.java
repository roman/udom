package com.romansl.udom;

import com.romansl.ranges.Range;
import com.romansl.ranges.Ranges;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

class Leaf extends Node {
    public Leaf(final @NotNull String name, final @NotNull Map<String, String> attributes, final String text) {
        super(name, attributes, text);
    }

    @NotNull
    @Override
    public Node optNode(final String name) {
        return NONE;
    }

    @NotNull
    @Override
    public Node optNode(final int index) {
        return NONE;
    }

    @NotNull
    @Override
    public Node getNode(final int index) {
        throw new RuntimeException();
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public Range<Node> range() {
        return Ranges.emptyRange();
    }
}
