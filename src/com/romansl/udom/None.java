package com.romansl.udom;

import com.romansl.ranges.Range;
import com.romansl.ranges.Ranges;

import org.jetbrains.annotations.NotNull;
import java.util.Collections;

class None extends Node {
    public None() {
        super("", Collections.<String, String>emptyMap(), "");
    }

    @NotNull
    @Override
    public Node optNode(final String name) {
        return Node.NONE;
    }

    @NotNull
    @Override
    public Node optNode(final int index) {
        return Node.NONE;
    }

    @NotNull
    @Override
    public Node getNode(final int index) {
        throw new RuntimeException();
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public Range<Node> range() {
        return Ranges.emptyRange();
    }
}
